<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class Area2Controller extends Controller
{

    public function __construct()
    {
        //deklarasi arrray, area ini regional apa saja
        $this->regional = ['regional-03','regional-04']; // regional 8 , 9 , 11
    }

    public function index() {

        $urls = [];
        $channels = ['UCYfblrhI1-tr6leVANholgQ','UC2ZouGm7-2_LRX1TQVbs0_g','UCef1-8eOpJgud7szVPlZQAQ','UCoSkllfpgmFHtbVK835QaQg'];
        $api_key = 'AIzaSyDjiDvrUXpvP79xlpDebzqJPzgPjbo1Qu8'; // This is your google project API KEY with youtube api enabled

        foreach($channels as $item) {

            // $YouTubeLive = new EmbedYoutubeLiveStreaming($item, $api_key);

            // if(!$YouTubeLive->isLive)
            // {
            //     // var_dump($YouTubeLive->objectResponse);
            // }
            // else
            // {
            //     array_push($urls, $YouTubeLive->embedCode());
            // }

            array_push($urls, "");
        }

        return view('area2', compact('urls'));
    }

    public function get_total_payload_user_today() {

        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');

        $data = DB::table('poi_cumulative')->select(DB::raw('DATE_FORMAT(starttime, "%H:%i") as starttime'),DB::raw("sum(cum_trafficmbyte)/1000 as payload"),DB::raw("sum(cum_subs) as user"))
        ->where('starttime', 'like', date('Y-m-d').'%')
        ->whereIn('regional',$this->regional)
        ->groupBy('starttime')
        ->get();

        return response()->json($data);
    }

    public function get_total_payload_user_by_app() {

        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');

        $data = DB::table('poi_cumulative_per_apps')->select('application',DB::raw("sum(cum_trafficmbyte)/1000 as payload"),DB::raw("sum(cum_subs) as user"))
        ->where('starttime', '=', DB::raw("(select max(`starttime`) from poi_cumulative_per_apps)"))
        ->whereIn('regional',$this->regional)
        ->groupBy('application')
        ->orderBy('user', 'DESC')
        ->get();


        //get last update
        $get_last_updated = DB::table('poi_cumulative_per_apps')->select('starttime as updated_at')
        ->where('starttime', DB::raw("(select max(`starttime`) from poi_cumulative_per_apps)"))
        ->limit(1)
        ->get();
        $last_updated = date("Y-m-d H:i:s", strtotime($get_last_updated[0]->updated_at));

        return response()->json(
            [
                'data' => $data,
                'last_update' => $last_updated
            ]
        );
    }

    //konten

    public function get_total_payload_and_user() {

        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');

        //proses
        $distinct_poi = DB::table('poi_cumulative_per_apps')->select('superpoi')
        ->where('starttime', 'like', date('Y-m-d').'%')
        ->whereIn('regional',['regional-03','regional-04'])
        ->distinct('superpoi')
        ->orderBy('regional', 'ASC')
        ->get();

        $data = [];
        foreach($distinct_poi as $item) {

            $items = DB::table('poi_cumulative_per_apps')->select('regional','superpoi','application',DB::raw("cum_trafficmbyte/1000 as payload"),DB::raw("cum_subs as user"))
            ->where('starttime', '=', DB::raw("(select max(`starttime`) from poi_cumulative_per_apps)"))
            ->where('superpoi','=',$item->superpoi)
            ->groupBy('regional','superpoi','application')
            ->orderBy('user', 'DESC')
            ->limit(5)
            ->get();

            // dd($items);

            foreach($items as $item) {
                array_push($data, $item);
            }
        }

        $array_detail = [];
        $application = [];
        $payload = [];
        $user = [];
        foreach($distinct_poi as $item_poi) {
            foreach($data as $item) {
                if($item_poi->superpoi == $item->superpoi) {
                    array_push($application, $item->application);
                    array_push($payload, round($item->payload, 2));
                    array_push($user, (int)$item->user);
                }
            }
            array_push($array_detail, ['superpoi'=>$item_poi->superpoi,'application'=>$application,'payload_by_app'=>$payload,'user_by_app'=>$user]);
            $application = [];
            $payload = [];
            $user = [];
        }


        //get last update
        $get_last_updated = DB::table('poi')->select('starttime as updated_at')
        ->where('starttime', DB::raw("(select max(`starttime`) from poi)"))
        ->limit(1)
        ->get();
        $last_updated = date("Y-m-d H:i:s", strtotime($get_last_updated[0]->updated_at));
        $time = date("h:i", strtotime($last_updated));

        //GET URL CCTV
        $data_url_cctv = DB::table('poi_url_cctv')->select('superpoi','url_cctv','icon');

        //sub query - get average baseline today
        $average_baseline_today = DB::table('poi_summary_average_baseline_today')->select('poi','traffic')
        ->where('time', 'like', '%'.$time.'%')
        ->groupBy('poi','traffic');

        //GET LATEST MINUTS
        $data_latest_minuts = DB::table('poi')->select('superpoi','regional',DB::raw("sum(user) as user"),DB::raw("sum(payload)/1000 as payload"))
        ->where('starttime', \DB::raw("(select max(`starttime`) from poi)"))
        ->whereIn('regional',['regional-03','regional-04'])
        ->groupBy('superpoi','regional');

        //sub query - get average baseline today
        $average_baseline_latest_minuts = DB::table('poi_summary_average_baseline_last_thirty_minutes')->select('poi','traffic')
        ->where('time', 'like', '%'.$time.'%')
        ->groupBy('poi','traffic');

        $main_data = DB::table('poi_cumulative as poi')
        ->select('poi.superpoi','poi.regional','data_url_cctv.url_cctv','data_url_cctv.icon',
         //today
         DB::raw("poi.cum_subs as today_total_user"),
         DB::raw("round((poi.cum_trafficmbyte)/1000,2) as today_total_payload"),
         DB::raw("average_baseline_today.traffic as today_payload_average_baseline"),
         DB::raw("(round((poi.cum_trafficmbyte)/1000,2) - average_baseline_today.traffic) as today_payload_delta_baseline"),
         DB::raw("
                     (
                         CASE average_baseline_today.traffic WHEN 0 THEN 0 ELSE round((((round((poi.cum_trafficmbyte)/1000,2) - average_baseline_today.traffic) / average_baseline_today.traffic) * 100),2)  END
                     )
                  as today_payload_growth_baseline"), //growth = ((actual - baseline) / 100) * 100)
        //latest minuts
        DB::raw("latest_minuts.user as latest_minuts_total_user"),  // latest minuts dari sub query
        DB::raw("round(latest_minuts.payload,2) as latest_minuts_total_payload"), //sudah dibagi 1000 di sub query
        DB::raw("average_baseline_latest_minuts.traffic as latest_minuts_payload_average_baseline"),
        DB::raw("(round(latest_minuts.payload, 2) - average_baseline_latest_minuts.traffic) as latest_minuts_payload_delta_baseline"), //delta = actual - baseline
        DB::raw("
                    (
                        CASE average_baseline_latest_minuts.traffic WHEN 0 THEN 0 ELSE round((((round(latest_minuts.payload, 2) - average_baseline_latest_minuts.traffic) / average_baseline_latest_minuts.traffic) * 100),2) END
                    )
                 as latest_minuts_payload_growth_baseline")) //growth = ((actual - baseline) / 100) * 100)
        ->joinSub($data_latest_minuts, 'latest_minuts', function ($join) {
            $join->on('poi.superpoi', '=', 'latest_minuts.superpoi');
        })
        ->joinSub($data_url_cctv, 'data_url_cctv', function ($join) {
            $join->on('poi.superpoi', '=', 'data_url_cctv.superpoi');
        })
        ->leftJoinSub($average_baseline_today, 'average_baseline_today', function ($join) {
            $join->on('poi.superpoi', '=', 'average_baseline_today.poi');
        })
        ->leftJoinSub($average_baseline_latest_minuts, 'average_baseline_latest_minuts', function ($join) {
            $join->on('poi.superpoi', '=', 'average_baseline_latest_minuts.poi');
        })
        ->whereIn('poi.regional',['regional-03','regional-04'])
        ->where('poi.starttime', \DB::raw("(select max(`starttime`) from poi_cumulative)"))
        ->groupBy('poi.superpoi','poi.regional','latest_minuts.user','latest_minuts.payload')
        ->get();

        // dd($main_data);

        $data = [];
        foreach($array_detail as $item_detail) {

            foreach($main_data as $item) {

                if($item_detail['superpoi'] == $item->superpoi) {

                    $array = [
                        'superpoi'  => $item->superpoi,
                        'regional'  => $item->regional,
                        'url_cctv'  => $item->url_cctv,
                        'icon'  => $item->icon,
                        'today_total_user'  => (int)$item->today_total_user,
                        'today_total_payload'  => $item->today_total_payload,
                        'today_payload_average_baseline'  => $item->today_payload_average_baseline,
                        'today_payload_delta_baseline'  => $item->today_payload_delta_baseline,
                        'today_payload_growth_baseline'  => $item->today_payload_growth_baseline,
                        'latest_minuts_total_user'  => (int)$item->latest_minuts_total_user,
                        'latest_minuts_total_payload'  => $item->latest_minuts_total_payload,
                        'latest_minuts_payload_average_baseline'  => $item->latest_minuts_payload_average_baseline,
                        'latest_minuts_payload_delta_baseline'  => $item->latest_minuts_payload_delta_baseline,
                        'latest_minuts_payload_growth_baseline'  => $item->latest_minuts_payload_growth_baseline,
                        'highest_application_usage' => $item_detail['application'],
                        'total_payload_by_app' => $item_detail['payload_by_app'],
                        'total_user_by_app' => $item_detail['user_by_app']
                    ];

                    array_push($data, $array);

                }
            }
        }

        return response()->json($data);
    }
}
