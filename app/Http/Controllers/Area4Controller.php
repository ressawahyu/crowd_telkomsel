<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class Area4Controller extends Controller
{

    public function __construct()
    {
        //deklarasi arrray, area ini regional apa saja
        $this->regional = ['regional-08','regional-09']; // regional 8 , 9 , 11
    }

    public function index() {

        $urls = [];
        $channels = ['UCYfblrhI1-tr6leVANholgQ','UC2ZouGm7-2_LRX1TQVbs0_g','UCef1-8eOpJgud7szVPlZQAQ','UCoSkllfpgmFHtbVK835QaQg'];
        $api_key = 'AIzaSyDjiDvrUXpvP79xlpDebzqJPzgPjbo1Qu8'; // This is your google project API KEY with youtube api enabled

        foreach($channels as $item) {

            // $YouTubeLive = new EmbedYoutubeLiveStreaming($item, $api_key);

            // if(!$YouTubeLive->isLive)
            // {
            //     // var_dump($YouTubeLive->objectResponse);
            // }
            // else
            // {
            //     array_push($urls, $YouTubeLive->embedCode());
            // }

            array_push($urls, "");
        }

        return view('layout_3_content', compact('urls'));
    }

    public function get_total_payload_user_today() {

        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');

        $data = DB::table('poi_cumulative')->select(DB::raw('DATE_FORMAT(starttime, "%H:%i") as starttime'),DB::raw("sum(cum_trafficmbyte)/1000 as payload"),DB::raw("sum(cum_subs) as user"))
        ->where('starttime', 'like', date('Y-m-d').'%')
        ->whereIn('regional',$this->regional)
        ->groupBy('starttime')
        ->get();

        // $date = date('Y-m-d');
        // $data = DB::connection('mysql')->select(
        // "SELECT DATE_FORMAT(c.starttime, '%H:%i') as starttime, SUM(c.payload) AS payload, sum(c.`user`) AS `user` FROM (
        // SELECT a.starttime AS starttime, round(SUM(a.cum_trafficmbyte/1000),2) AS payload, SUM(a.cum_subs) AS `user`
        // FROM poi_cumulative a
        // WHERE a.starttime LIKE '$date%'
        // AND a.regional IN ('regional-08','regional-09')
        // GROUP BY a.starttime
        // UNION
        // SELECT b.starttime AS startime, ROUND((b.cum_trafficmbyte/1000000),2) AS payload, b.cum_subs as `user` FROM poi_raw_data_lapangan_merdeka_ambon b
        // WHERE b.starttime LIKE '$date%'
        // AND b.application = 'total'
        // GROUP BY b.starttime ) c
        // GROUP BY c.starttime "
        // );

        return response()->json($data);
    }

    public function get_total_payload_user_by_app() {

        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');

        // $data = DB::table('poi_cumulative_per_apps')->select('application',DB::raw("sum(cum_trafficmbyte)/1000 as payload"),DB::raw("sum(cum_subs) as user"))
        // ->where('starttime', '=', DB::raw("(select max(`starttime`) from poi_cumulative_per_apps)"))
        // ->whereIn('regional',$this->regional)
        // ->groupBy('application')
        // ->orderBy('user', 'DESC')
        // ->get();

        $data = DB::connection('mysql')->select(
        "SELECT c.application AS application, SUM(c.payload) AS payload, sum(c.`user`) AS `user` FROM (
            SELECT a.application AS application, round(SUM(a.cum_trafficmbyte)/1000,2) AS payload, SUM(a.cum_subs) AS `user`
            FROM poi_cumulative_per_apps a
            WHERE a.starttime = (SELECT MAX(starttime) FROM poi_cumulative_per_apps)
            AND a.regional IN ('regional-08','regional-09')
            GROUP BY a.application
            UNION
            SELECT b.application AS application, round(SUM(b.cum_trafficmbyte)/1000000,2) AS payload, SUM(b.cum_subs) AS `user`
            FROM poi_raw_data_lapangan_merdeka_ambon b
            WHERE b.starttime = (SELECT MAX(starttime) FROM poi_raw_data_lapangan_merdeka_ambon)
            AND b.application NOT IN ('total')
            GROUP BY b.application
            ) c
            GROUP BY c.application
            ORDER BY `user` DESC
            LIMIT 10
            "
        );

        return response()->json($data);
    }


    public function get_total_payload_user_by_regional_by_app() {

        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');

        $distinct_poi = DB::table('poi_cumulative_per_apps')->select('superpoi')
        ->where('starttime', 'like', date('Y-m-d').'%')
        ->whereIn('regional',['regional-08','regional-09'])
        ->distinct('superpoi')
        ->orderBy('regional', 'ASC')
        ->get()->toArray();
        // dd($distinct_poi);

        $data = [];
        foreach($distinct_poi as $item) {

            $items = DB::table('poi_cumulative_per_apps')->select('regional','superpoi','application',DB::raw("cum_trafficmbyte/1000 as payload"),DB::raw("cum_subs as user"))
            ->where('starttime', '=', DB::raw("(select max(`starttime`) from poi_cumulative_per_apps)"))
            ->where('superpoi','=',$item->superpoi)
            ->groupBy('regional','superpoi','application')
            ->orderBy('user', 'DESC')
            ->limit(5)
            ->get();

            foreach($items as $item) {
                array_push($data, $item);
            }
        }

        //ADD ITEMS
        //regional 11
        $items_regional = DB::table('poi_raw_data_lapangan_merdeka_ambon')->select('regional','superpoi','application',DB::raw("current_payload/1000000 as payload"),DB::raw("current_sub as user"))
        ->where('starttime', '=', DB::raw("(select max(`starttime`) from poi_raw_data_lapangan_merdeka_ambon)"))
        ->whereNotIn('application',['total'])
        ->groupBy('starttime', 'application')
        ->orderBy('current_sub', 'DESC')
        ->limit(5)
        ->get();

        if($items_regional != NULL) {
            foreach($items_regional as $item) {
                array_push($data, $item);
            }
        }

        array_push($distinct_poi, ['superpoi'=>'LAPANGAN MERDEKA AMBON']);

        // //check hasil query data, apabila isinya tidak sama dengan tiga, akan dibuatkan data dummy untuk layoutin agar tidak kosong dihalaman
        // $distinctArray = array_unique(array_column($data, 'regional'));

        // //membandingkan regional
        // $result_compare_regional = array_diff($this->regional, $distinctArray);
        // foreach($result_compare_regional as $item) {
        //     $add_Item = [
        //         'regional' => $item,
        //         'superpoi' => 'LOCATION NOT FOUND',
        //         'application' => 'NotFound',
        //         'payload' => 0,
        //         'user' => 0
        //     ];
        //     array_push($data, $add_Item);
        //     array_push($distinct_poi, ['superpoi' => 'LOCATION NOT FOUND']);
        // }

        $datas = [
            'data'  => $data,
            'poi'   => $distinct_poi
        ];

        return response()->json($datas);
    }

        public function get_total_payload_and_user() {

        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');

        //get last update
        $get_last_updated = DB::table('poi')->select('starttime as updated_at')
        ->where('starttime', DB::raw("(select max(`starttime`) from poi)"))
        ->limit(1)
        ->get();
        $last_updated = date("Y-m-d H:i:s", strtotime($get_last_updated[0]->updated_at));
        $time = date("H:i", strtotime($last_updated));

        // dd($get_last_updated);

        //sub query - get url cctv
        $data_url_cctv = DB::table('poi_url_cctv')->select('superpoi','url_cctv','icon');

        //sub query - get average baseline today
        $average_baseline_today = DB::table('poi_summary_average_baseline_today')->select('poi','traffic')
        ->where('time', 'like', '%'.$time.'%')
        ->groupBy('poi','traffic');

        //sub query - get data latest_minuts
        $latest_minuts = DB::table('poi')->select('superpoi','regional',DB::raw("sum(user) as user"),DB::raw("sum(payload)/1000 as payload"))
        ->where('starttime', \DB::raw("(select max(`starttime`) from poi)"))
        ->whereIn('regional',$this->regional)
        ->groupBy('superpoi','regional');

        //sub query - get average baseline today
        $average_baseline_latest_minuts = DB::table('poi_summary_average_baseline_last_thirty_minutes')->select('poi','traffic')
        ->where('time', 'like', '%'.$time.'%')
        ->groupBy('poi','traffic');


        //main query
        $datas = DB::table('poi_cumulative as poi')
        ->select('poi.superpoi','poi.regional','data_url_cctv.url_cctv','data_url_cctv.icon',
         //today
         DB::raw("poi.cum_subs as today_total_user"),
         DB::raw("round((poi.cum_trafficmbyte)/1000,2) as today_total_payload"),
         DB::raw("average_baseline_today.traffic as today_payload_average_baseline"),
         DB::raw("(round((poi.cum_trafficmbyte)/1000,2) - average_baseline_today.traffic) as today_payload_delta_baseline"),
         DB::raw("
                     (
                         CASE average_baseline_today.traffic WHEN 0 THEN 0 ELSE round((((round((poi.cum_trafficmbyte)/1000,2) - average_baseline_today.traffic) / average_baseline_today.traffic) * 100),2)  END
                     )
                  as today_payload_growth_baseline"), //growth = ((actual - baseline) / 100) * 100)
         //latest minuts
         DB::raw("latest_minuts.user as latest_minuts_total_user"),  // latest minuts dari sub query
         DB::raw("round(latest_minuts.payload,2) as latest_minuts_total_payload"), //sudah dibagi 1000 di sub query
         DB::raw("average_baseline_latest_minuts.traffic as latest_minuts_payload_average_baseline"),
         DB::raw("(round(latest_minuts.payload, 2) - average_baseline_latest_minuts.traffic) as latest_minuts_payload_delta_baseline"), //delta = actual - baseline
         DB::raw("
                     (
                         CASE average_baseline_latest_minuts.traffic WHEN 0 THEN 0 ELSE round((((round(latest_minuts.payload, 2) - average_baseline_latest_minuts.traffic) / average_baseline_latest_minuts.traffic) * 100),2) END
                     )
                  as latest_minuts_payload_growth_baseline")) //growth = ((actual - baseline) / 100) * 100)
        ->joinSub($latest_minuts, 'latest_minuts', function ($join) {
            $join->on('poi.superpoi', '=', 'latest_minuts.superpoi');
        })
        ->joinSub($data_url_cctv, 'data_url_cctv', function ($join) {
            $join->on('poi.superpoi', '=', 'data_url_cctv.superpoi');
        })
        ->leftJoinSub($average_baseline_today, 'average_baseline_today', function ($join) {
            $join->on('poi.superpoi', '=', 'average_baseline_today.poi');
        })
        ->leftJoinSub($average_baseline_latest_minuts, 'average_baseline_latest_minuts', function ($join) {
            $join->on('poi.superpoi', '=', 'average_baseline_latest_minuts.poi');
        })
        ->whereIn('poi.regional',$this->regional)
        ->where('poi.starttime', \DB::raw("(select max(`starttime`) from poi_cumulative)"))
        ->groupBy('poi.superpoi','poi.regional','latest_minuts.user','latest_minuts.payload')
        ->orderBy('poi.regional','ASC')
        ->get()->toArray();

        //Regional 11 memiliki format tabel yang berbeda, sehingga untuk query dipisah
        //get data lapangan merdeka ambon

        $get_data_lapangan_merdeka_ambon = DB::table('poi_raw_data_lapangan_merdeka_ambon as poi')
        ->select('poi.superpoi','data_url_cctv.url_cctv','data_url_cctv.icon',
         //today
         DB::raw("poi.cum_subs as today_total_user"),
         DB::raw("round((poi.cum_trafficmbyte/1000000),2) as today_total_payload"),
         DB::raw("average_baseline_today.traffic as today_payload_average_baseline"),
         DB::raw("(round((poi.cum_trafficmbyte/1000000),2) - average_baseline_today.traffic) as today_payload_delta_baseline"),
         DB::raw("
                     (
                         CASE average_baseline_today.traffic WHEN 0 THEN 0 ELSE round((((round((poi.cum_trafficmbyte/1000000),2) - average_baseline_today.traffic) / average_baseline_today.traffic) * 100),2)  END
                     )
                  as today_payload_growth_baseline"), //growth = ((actual - baseline) / 100) * 100)
         //latest minuts
         DB::raw("poi.current_sub as latest_minuts_total_user"),  // latest minuts dari sub query
         DB::raw("round((poi.current_payload/1000000),2) as latest_minuts_total_payload"), //sudah dibagi 1000 di sub query
         DB::raw("average_baseline_latest_minuts.traffic as latest_minuts_payload_average_baseline"),
         DB::raw("(round((poi.current_payload/1000000),2) - average_baseline_latest_minuts.traffic) as latest_minuts_payload_delta_baseline"), //delta = actual - baseline
         DB::raw("
                     (
                         CASE average_baseline_latest_minuts.traffic WHEN 0 THEN 0 ELSE round((((round((poi.current_payload/1000000),2) - average_baseline_latest_minuts.traffic) / average_baseline_latest_minuts.traffic) * 100),2) END
                     )
                  as latest_minuts_payload_growth_baseline")) //growth = ((actual - baseline) / 100) * 100)

        ->joinSub($data_url_cctv, 'data_url_cctv', function ($join) {
            $join->on('poi.superpoi', '=', 'data_url_cctv.superpoi');
        })
        ->leftJoinSub($average_baseline_today, 'average_baseline_today', function ($join) {
            $join->on('poi.superpoi', '=', 'average_baseline_today.poi');
        })
        ->leftJoinSub($average_baseline_latest_minuts, 'average_baseline_latest_minuts', function ($join) {
            $join->on('poi.superpoi', '=', 'average_baseline_latest_minuts.poi');
        })
        ->where('poi.starttime', \DB::raw("(select max(`starttime`) from poi_raw_data_lapangan_merdeka_ambon)"))
        ->where('poi.application','LIKE','total')
        ->groupBy('poi.starttime','poi.application')
        ->get();

        // dd($get_data_lapangan_merdeka_ambon);
        if($get_data_lapangan_merdeka_ambon != NULL) {
            foreach($get_data_lapangan_merdeka_ambon as $item) {
                $add_Item = [
                    'superpoi' => $item->superpoi,
                    'regional' => 'regional-11',
                    'url_cctv' => $item->url_cctv,
                    'icon' => $item->icon,
                    'today_total_user' => $item->today_total_user,
                    'today_total_payload' => $item->today_total_payload,
                    'today_payload_average_baseline' => $item->today_payload_average_baseline,
                    'today_payload_delta_baseline' => $item->today_payload_delta_baseline,
                    'today_payload_growth_baseline' => $item->today_payload_growth_baseline,
                    'latest_minuts_total_user' => $item->latest_minuts_total_user,
                    'latest_minuts_total_payload' => $item->latest_minuts_total_payload,
                    'latest_minuts_payload_average_baseline' => $item->latest_minuts_payload_average_baseline,
                    'latest_minuts_payload_delta_baseline' => $item->latest_minuts_payload_delta_baseline,
                    'latest_minuts_payload_growth_baseline' => $item->latest_minuts_payload_growth_baseline
                ];
                // dd($add_Item);
                array_push($datas, $add_Item);
            }
        }
        // dd($datas);

        //check hasil query data, apabila isinya tidak sama dengan sejumlah regional pada area, akan dibuatkan data dummy untuk layoutin agar tidak kosong dihalaman
        $result_regional = [];
        foreach($datas as $item) {

            if(is_object($item)) {
                array_push($result_regional, $item->regional);
            } else {
                array_push($result_regional, $item['regional']);
            }
        }

        // dd($result_regional);

        //membandingkan regional
        $result_compare_regional = array_diff($this->regional, $result_regional);
        foreach($result_compare_regional as $item) {
            $add_Item = [
                'superpoi' => 'LOCATION NOT FOUND',
                'regional' => $item,
                'url_cctv' => '',
                'icon' => '<img src="https://img.icons8.com/bubbles/50/000000/city.png">',
                'today_total_user' => 0,
                'today_total_payload' => 0,
                'today_payload_average_baseline' => 0,
                'today_payload_delta_baseline' => 0,
                'today_payload_growth_baseline' => 0,
                'latest_minuts_total_user' => 0,
                'latest_minuts_total_payload' => 0,
                'latest_minuts_payload_average_baseline' => 0,
                'latest_minuts_payload_delta_baseline' => 0,
                'latest_minuts_payload_growth_baseline' => 0
            ];
            array_push($datas, $add_Item);
        }

        //result
        $data = [
            'data'=>$datas,
            'updated_at'=> $last_updated
        ];

        return response()->json($data);
    }
}
