<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SummaryAvgBaselineToday extends Controller
{
    public function index() {

        ini_set('max_execution_time', 86400);
        ini_set('memory_limit', '10240M');

        $query = "
        SELECT (CASE
        WHEN `time` LIKE '% 00:00%' THEN '00:00'
        WHEN `time` LIKE '% 00:30%' THEN '00:30'
        WHEN `time` LIKE '% 01:00%' THEN '01:00'
        WHEN `time` LIKE '% 01:30%' THEN '01:30'
        WHEN `time` LIKE '% 02:00%' THEN '02:00'
        WHEN `time` LIKE '% 02:30%' THEN '02:30'
        WHEN `time` LIKE '% 03:00%' THEN '03:00'
        WHEN `time` LIKE '% 03:30%' THEN '03:30'
        WHEN `time` LIKE '% 04:00%' THEN '04:00'
        WHEN `time` LIKE '% 04:30%' THEN '04:30'
        WHEN `time` LIKE '% 05:00%' THEN '05:00'
        WHEN `time` LIKE '% 05:30%' THEN '05:30'
        WHEN `time` LIKE '% 06:00%' THEN '06:00'
        WHEN `time` LIKE '% 06:30%' THEN '06:30'
        WHEN `time` LIKE '% 07:00%' THEN '07:00'
        WHEN `time` LIKE '% 07:30%' THEN '07:30'
        WHEN `time` LIKE '% 08:00%' THEN '08:00'
        WHEN `time` LIKE '% 08:30%' THEN '08:30'
        WHEN `time` LIKE '% 09:00%' THEN '09:00'
        WHEN `time` LIKE '% 09:30%' THEN '09:30'
        WHEN `time` LIKE '% 10:00%' THEN '10:00'
        WHEN `time` LIKE '% 10:30%' THEN '10:30'
        WHEN `time` LIKE '% 11:00%' THEN '11:00'
        WHEN `time` LIKE '% 11:30%' THEN '11:30'
        WHEN `time` LIKE '% 12:00%' THEN '12:00'
        WHEN `time` LIKE '% 12:30%' THEN '12:30'
        WHEN `time` LIKE '% 13:00%' THEN '13:00'
        WHEN `time` LIKE '% 13:30%' THEN '13:30'
        WHEN `time` LIKE '% 14:00%' THEN '14:00'
        WHEN `time` LIKE '% 14:30%' THEN '14:30'
        WHEN `time` LIKE '% 15:00%' THEN '15:00'
        WHEN `time` LIKE '% 15:30%' THEN '15:30'
        WHEN `time` LIKE '% 16:00%' THEN '16:00'
        WHEN `time` LIKE '% 16:30%' THEN '16:30'
        WHEN `time` LIKE '% 17:00%' THEN '17:00'
        WHEN `time` LIKE '% 17:30%' THEN '17:30'
        WHEN `time` LIKE '% 18:00%' THEN '18:00'
        WHEN `time` LIKE '% 18:30%' THEN '18:30'
        WHEN `time` LIKE '% 19:00%' THEN '19:00'
        WHEN `time` LIKE '% 19:30%' THEN '19:30'
        WHEN `time` LIKE '% 20:00%' THEN '20:00'
        WHEN `time` LIKE '% 20:30%' THEN '20:30'
        WHEN `time` LIKE '% 21:00%' THEN '21:00'
        WHEN `time` LIKE '% 21:30%' THEN '21:30'
        WHEN `time` LIKE '% 22:00%' THEN '22:00'
        WHEN `time` LIKE '% 22:30%' THEN '22:30'
        WHEN `time` LIKE '% 23:00%' THEN '23:00'
        WHEN `time` LIKE '% 23:30%' THEN '23:30'
        END) AS `time`, `poi`, format(AVG(num_user),0) AS `user`, format(AVG(traffic_mode),0) AS `traffic`
        FROM poi_raw_data_baseline_today
        GROUP BY
        (CASE
        WHEN `time` LIKE '% 00:00%' THEN '00:00'
        WHEN `time` LIKE '% 00:30%' THEN '00:30'
        WHEN `time` LIKE '% 01:00%' THEN '01:00'
        WHEN `time` LIKE '% 01:30%' THEN '01:30'
        WHEN `time` LIKE '% 02:00%' THEN '02:00'
        WHEN `time` LIKE '% 02:30%' THEN '02:30'
        WHEN `time` LIKE '% 03:00%' THEN '03:00'
        WHEN `time` LIKE '% 03:30%' THEN '03:30'
        WHEN `time` LIKE '% 04:00%' THEN '04:00'
        WHEN `time` LIKE '% 04:30%' THEN '04:30'
        WHEN `time` LIKE '% 05:00%' THEN '05:00'
        WHEN `time` LIKE '% 05:30%' THEN '05:30'
        WHEN `time` LIKE '% 06:00%' THEN '06:00'
        WHEN `time` LIKE '% 06:30%' THEN '06:30'
        WHEN `time` LIKE '% 07:00%' THEN '07:00'
        WHEN `time` LIKE '% 07:30%' THEN '07:30'
        WHEN `time` LIKE '% 08:00%' THEN '08:00'
        WHEN `time` LIKE '% 08:30%' THEN '08:30'
        WHEN `time` LIKE '% 09:00%' THEN '09:00'
        WHEN `time` LIKE '% 09:30%' THEN '09:30'
        WHEN `time` LIKE '% 10:00%' THEN '10:00'
        WHEN `time` LIKE '% 10:30%' THEN '10:30'
        WHEN `time` LIKE '% 11:00%' THEN '11:00'
        WHEN `time` LIKE '% 11:30%' THEN '11:30'
        WHEN `time` LIKE '% 12:00%' THEN '12:00'
        WHEN `time` LIKE '% 12:30%' THEN '12:30'
        WHEN `time` LIKE '% 13:00%' THEN '13:00'
        WHEN `time` LIKE '% 13:30%' THEN '13:30'
        WHEN `time` LIKE '% 14:00%' THEN '14:00'
        WHEN `time` LIKE '% 14:30%' THEN '14:30'
        WHEN `time` LIKE '% 15:00%' THEN '15:00'
        WHEN `time` LIKE '% 15:30%' THEN '15:30'
        WHEN `time` LIKE '% 16:00%' THEN '16:00'
        WHEN `time` LIKE '% 16:30%' THEN '16:30'
        WHEN `time` LIKE '% 17:00%' THEN '17:00'
        WHEN `time` LIKE '% 17:30%' THEN '17:30'
        WHEN `time` LIKE '% 18:00%' THEN '18:00'
        WHEN `time` LIKE '% 18:30%' THEN '18:30'
        WHEN `time` LIKE '% 19:00%' THEN '19:00'
        WHEN `time` LIKE '% 19:30%' THEN '19:30'
        WHEN `time` LIKE '% 20:00%' THEN '20:00'
        WHEN `time` LIKE '% 20:30%' THEN '20:30'
        WHEN `time` LIKE '% 21:00%' THEN '21:00'
        WHEN `time` LIKE '% 21:30%' THEN '21:30'
        WHEN `time` LIKE '% 22:00%' THEN '22:00'
        WHEN `time` LIKE '% 22:30%' THEN '22:30'
        WHEN `time` LIKE '% 23:00%' THEN '23:00'
        WHEN `time` LIKE '% 23:30%' THEN '23:30'
        END), `poi`
        ";
        $getData = DB::connection('mysql')
        ->select($query);

        foreach($getData as $item)
        {

            $insertSummary = DB::connection('mysql')
            ->table('poi_summary_average_baseline_today')
            ->insert([
                'time'      => $item->time,
                'poi'       => $item->poi,
                'user'      => str_replace(',','',$item->user),
                'traffic'   => str_replace(',','',$item->traffic),
            ]);
        }

    }
}
