<!DOCTYPE html>
<html lang="en">
<head>
  <title>@yield('title')</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="{{url('assets/css/bootstrap.min.css')}}"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="{{url('assets/js/bootstrap.min.js')}}"></script>
  <script src="https://code.highcharts.com/highcharts.js"></script>

  <link rel="stylesheet" type="text/css" href="{{url('assets/css/fontawesome/css/fontawesome.min.css')}}"/>
  <link rel="stylesheet" type="text/css" href="{{url('assets/css/fontawesome/css/brands.css')}}"/>
  <link rel="stylesheet" type="text/css" href="{{url('assets/css/fontawesome/css/solid.css')}}"/>
  {{-- <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> --}}
  <link rel="shortcut icon" href="{{url('/images/icon_telkomsel.png')}}">
</head>

<style>
    html, body { height: 100%; width: 100%; font-size:0.8vw; background:#111a21; color:#fff }

    div { height:100%; }

    h6 {
        padding:5px;
    }

    .col, .col-1, .col-10, .col-11, .col-12, .col-2, .col-3, .col-4, .col-5, .col-6, .col-7, .col-8, .col-9, .col-auto, .col-lg, .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-auto, .col-md, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-auto, .col-sm, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-auto, .col-xl, .col-xl-1, .col-xl-10, .col-xl-11, .col-xl-12, .col-xl-2, .col-xl-3, .col-xl-4, .col-xl-5, .col-xl-6, .col-xl-7, .col-xl-8, .col-xl-9, .col-xl-auto {
    position: relative;
    width: 100%;
    padding : 10px;
    }

    .navbar{
    -webkit-box-shadow: 4px 9px 25px -6px rgba(77,77,77,0.61);
    -moz-box-shadow: 4px 9px 25px -6px rgba(77,77,77,0.61);
    box-shadow: 4px 9px 25px -6px rgba(77,77,77,0.61);
    }

    ul.dropdown-menu{
        width: 11vw;
        border-radius: 0px;
        font-weight: 400;
        font-size: 0.9em;
        line-height: 16px;
        text-decoration: none;
    padding: 0px;
    list-style-type: none;
    -webkit-animation: mymove 0.5s;
    animation: mymove 0.5s;
        background-color: #fff;
        -webkit-box-shadow: 4px 9px 25px -6px rgba(77,77,77,0.61);
    -moz-box-shadow: 4px 9px 25px -6px rgba(77,77,77,0.61);
    box-shadow: 4px 9px 25px -6px rgba(77,77,77,0.61);
    }

    li.dropdown:hover ul.dropdown-menu{
        display: block;
    }

    .dropdown-menu li:hover a{
    color: #ec5626;
    }

    .dropdown-menu li{
    border-bottom: 2px solid #f61a2d;
    padding: 20px;
    display:block;
    }

    .dropdown-menu li a{
    color: #444;
    text-decoration: none;
    text-transform: capitalize;
    }

    @keyframes mymove {
        from {
        left:  100px;
            height: 0px;
            opacity: 0;
            border-bottom:0px;
        }
        to {
            height: auto;

        }
    }

    .navbar-nav a.nav-link{
        font-family: 'Roboto', sans-serif;
    text-transform: uppercase;
        padding: 0px !important;
    }

    ul.navbar-nav li.nav-item{
    margin: 0 20px;
    }

    ul.navbar-nav .nav-item:after {
        content: '';
        display: block;
        height: 3px;
        width: 0;
        transition: width .5s ease, background-color .5s ease;
    }

    .navbar-nav .nav-item:hover:after {
        width: 100%;
        background:#eb3719;
    }

    .bg-light {
        background: linear-gradient(90deg, rgba(219,210,210,1) 0%, rgba(172,155,155,1) 22%, rgba(248,22,42,1) 100%) !important;
    }

    .card {
    position: relative;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    min-width: 0;
    word-wrap: break-word;
    background-color: #fff;
    background-clip: border-box;
    border: 1px solid rgba(0,0,0,.125);
    border-radius: .25rem;
    background: #16202c;
    }

    .table td, .table th {
        padding: 0.5%;
        vertical-align: top;
        background-color:#233041;
    }

    .color-grey {
        color: #e8d2d2;
    }

    .color-white {
        color: #da3d3d;
    }

    .color-green {
        color: #3842d7;
    }

    .color-background {
        background:#16202c;
    }

    .card-color-label {
        background: #a7a9ac;
    }

    .user-app-icon {
   /*   width: 0.3rem;
      height: 1%; */
    }

    .table-dark {
    color: #fff;
    background-color: #25384e;
    }

    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px;
        max-width: 800px;
        margin: 1em auto;
    }

    #container {
        height: 450px;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }

    .background-gradient-title {
        background: #16202c;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #16202c, #304257, #16202c);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #16202c, #304257, #16202c); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }

    .background-gradient-card {
        background: #16202c;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #16202c, #304257, #16202c);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #16202c, #304257, #16202c); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }

    .background-gradient-chart {
        background: #df253c;  /* fallback for old browsers */
        background: -webkit-linear-gradient(to right, #df253c, #16202c);  /* Chrome 10-25, Safari 5.1-6 */
        background: linear-gradient(to right, #df253c, #16202c); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    }

    .highcharts-color-0 {
	fill: #7cb5ec;
	stroke: #7cb5ec;
    }

</style>

<body>
     {{-- <nav id="navbar-template" class="navbar navbar-expand-sm bg-light navbar-light">
                <ul class="navbar-nav">
                </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('area1') }}">Area 1</a>
                    </li>

                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('area2') }}">Area 2</a>
                    </li>

                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('area3') }}">Area 3</a>
                    </li>

                    <li class="nav-item">
                    <a class="nav-link" href="{{ route('area4') }}">Area 4</a>
                    </li>

                    <li class="nav-item dropdown"  >
                    <a class="nav-link " data-toggle="dropdown" href="#">Area</a>
                        <ul class="dropdown-menu">
                        <li><a href="#">Area 1</a></li>
                        <li><a href="#">Area 2</a></li>
                        <li><a href="#">Area 3</a></li>
                        <li><a href="#">Area 4</a></li>
                        </ul>
                    </li>
                </ul>
        </nav> --}}

        @yield('main-content')
</body>

@yield('script')

</html>
