@extends('layout.template')

@section('title','Dashboard Crowd | Area 2')

@section('main-content')

<div class="container-fluid">

    <div class="row" style="margin:0%; padding:0%;">

        <div class="col-md-2" style="padding:0%; height:100%; ">

                <div class="col-md-12" style="padding:15px; padding-left:0%; padding-right:0%; height:15%">
                    <div class="row " style="padding:0%; margin:0%">

                        <div class="col-md-12" style="padding:0%">
                            <div class="card card-color text-center">
                                <div class="card-body background-gradient-card" style="padding:15px;padding-left:0px;padding-right:0px">
                                    <div class="row" style="padding:2px; padding-left:60px; padding-right:30px">
                                        <img src="{{url('/images/telkomsel.png')}}"  width="50%" height="100%">
                                        <img src="{{url('/images/naru-icon2.png')}}"  width="50%" height="100%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="height:17.5%; padding:0%; padding-bottom:15px;">
                    <div class="card card-color">
                        <div class="card-body" style="padding:2px;">
                            <div id="chart_user" style="height:100%; width:100%;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="height:17.5%; padding:0%; padding-bottom:8px;">
                    <div class="card card-color">
                        <div class="card-body" style="padding:2px;">
                            <div id="chart_payload" style="height:100%; width:100%;"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="height:4.5%; padding-bottom:0%; padding-right:0%; padding-left:0%;">
                    <div class="row" style="padding-left:15px; padding-right:15px;">
                        <div class="col-md-6 text-center" style="padding-top:5px; padding-left:15px;background: #16202c;  /* fallback for old browsers */
                        background: -webkit-linear-gradient(to right, #353fc7, #16202c);  /* Chrome 10-25, Safari 5.1-6 */
                        background: linear-gradient(to right, #353fc7, #16202c); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
                        ">
                        <i class="fa fa-users" aria-hidden="true"></i> User
                    </div>

                    <div class="col-md-6 text-center" style="padding-top:5px; padding-left:15px;background: #16202c;  /* fallback for old browsers */
                    background: -webkit-linear-gradient(to left, #cb253b, #16202c);  /* Chrome 10-25, Safari 5.1-6 */
                    background: linear-gradient(to left, #cb253b, #16202c); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
                    ">
                    Payload <i class="fa fa-database" aria-hidden="true"></i>
                    </div>
                    </div>
                </div>

                <div class="col-md-12" style="height:40%; padding:0%; padding-bottom:0px; padding-top:12px;">
                    <div class="card card-color h-100" style="width:100%" >
                    <div class="card-body text-center h-100" style="padding-bottom:40px; padding:5px">
                    <center><label style="font-weight:bold; height:10%">Social Media User in This Area</label></center>
                    <table id="table_value" class="table table-dark table-striped w-100" style="text-align:center; height:93%">
                        <thead>
                            <tr>
                                <th>Media</th>
                                <th style="color: #90aef1;">User</th>
                                <th style="color: #ff828f;">Payload</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                    </div>
                    </div>
                </div>

                <div class="col-md-12" style="height:5%; padding-right:0%; padding-left:0%;" id="label_last_updated">
                    <div class="card card-color">
                    <div class="card-body text-right background-gradient-card" style="padding: 5px; font-size:12px; padding-right:10px">
                    </div>
                    </div>
                </div>

            </div>

            <div class="col-md-10" style="padding:0%; height:100%; ">

                <div class="row" style="padding:15px; padding-left:30px; padding-bottom:0%;" id="content_frame">

                {{-- <div class="col-md-6" style="height:50%; padding:0%; padding-left:15px; padding-bottom:15px">

                <div class="row" style="padding:0%; margin:0%; height:8%">

                <div class="col-md-12">

                </div>

                </div>

                <div class="row" style="padding:0%; margin:0%; height:92%">

                <div class="col-md-3" style="height:100%;">

                </div>


                <div class="col-md-9">

                    <div class="row">

                        <div class="col-md-12" style="height:80%;">

                        </div>

                        <div class="col-md-12" style="height:20%">

                        </div>

                    </div>

                </div>

                </div>

                </div> --}}

                </div>
        </div>
    </div>
</div>

@endsection


@section('script')

<script>

    //format number function
    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }


    $(document).ready(function () {

        //set URL
        var BaseUrl = {!! json_encode(url('/')) !!}
        var url = window.location.href;
        if(url.includes("area1")) {
            var area = 'area1';
            var title = 'AREA 1'
        } else if(url.includes("area2")) {
            var area = 'area2';
            var title = 'AREA 2'
        } else if(url.includes("area3")) {
            var area = 'area3';
            var title = 'AREA 3'
        } else if(url.includes("area")) {
            var area = 'area4';
            var title = 'AREA 4'
        }

        //chart
        get_total_payload_user_today();
        get_total_payload_user_by_app();

        //konten
        get_total_payload_and_user();


        function get_total_payload_and_user() {
            $.ajax({
                type: "GET",
                url: BaseUrl+'/'+area+'/get-total-payload-user',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {

                    $('#content_frame').html();

                    data.forEach(function(element)  {

                        var application_tracking = element.highest_application_usage;
                        var payload_tracking = element.total_payload_by_app;
                        var user_tracking = element.total_user_by_app;

                        if(element.regional == 'regional-01') {
                            element_regional = 'REGIONAL 01';
                            color_title = 'color:#daca92'
                        } else if (element.regional == 'regional-02') {
                            element_regional = 'REGIONAL 02';
                            color_title = 'color:#92daa8';
                        } else if (element.regional == 'regional-03') {
                            element_regional = 'REGIONAL 03';
                            color_title = 'color:#daca92'
                        } else if (element.regional == 'regional-04') {
                            element_regional = 'REGIONAL 04';
                            color_title = 'color:#92daa8';
                        } else if (element.regional == 'regional-05') {
                            element_regional = 'REGIONAL 05';
                            color_title = 'color:#daca92'
                        } else if (element.regional == 'regional-06') {
                            element_regional = 'REGIONAL 06';
                            color_title = 'color:#92daa8';
                        } else if (element.regional == 'regional-07') {
                            element_regional = 'REGIONAL 07';
                            color_title = 'color:#e188ff';
                        } else if (element.regional == 'regional-08') {
                            element_regional = 'REGIONAL 08';
                            color_title = 'color:#daca92'
                        } else if (element.regional == 'regional-09') {
                            element_regional = 'REGIONAL 09';
                            color_title = 'color:#92daa8';
                        } else if (element.regional == 'regional-10') {
                            element_regional = 'REGIONAL 10';
                            color_title = 'color:#e188ff';
                        } else if (element.regional == 'regional-11') {
                            element_regional = 'REGIONAL 11';
                            color_title = 'color:#e188ff';
                        } else {
                            element_regional = element.regional;
                        }

                        //arrow payload today
                        var today_payload_delta_baseline = "";
                        today_payload_delta_baseline += element.today_payload_delta_baseline;
                        var result_today_payload_delta_baseline = today_payload_delta_baseline.includes("-");
                        if(result_today_payload_delta_baseline === true) {
                            var icon_payload_today = '<i class="fas fa-arrow-down" style="color:#ff213c; font-size:25px; padding-top:25%;" data-toggle="tooltip" title="Baseline : '+formatNumber(Math.round(element.today_payload_average_baseline * 100) / 100)+'\nActual : '+formatNumber(Math.round(element.today_total_payload * 100) / 100)+'\nDelta : '+formatNumber(Math.round(element.today_payload_delta_baseline * 100)/100)+'\nGrowth : '+formatNumber(element.today_payload_growth_baseline)+' %"></i>';
                        } else {
                            var icon_payload_today = '<i class="fas fa-arrow-up" style="color:#21ff7a; font-size:25px; padding-top:25%;" data-toggle="tooltip" title="Baseline : '+formatNumber(Math.round(element.today_payload_average_baseline * 100) / 100)+'\nActual : '+formatNumber(Math.round(element.today_total_payload * 100) / 100)+'\nDelta : '+formatNumber(Math.round(element.today_payload_delta_baseline * 100)/100)+'\nGrowth : '+formatNumber(element.today_payload_growth_baseline)+' %"></i>';
                        }

                        //arrow payload latest minuts
                        var latest_minuts_payload_delta_baseline = "";
                        latest_minuts_payload_delta_baseline += element.latest_minuts_payload_delta_baseline;
                        var result_latest_minuts_payload_delta_baseline = latest_minuts_payload_delta_baseline.includes("-");
                        if(result_latest_minuts_payload_delta_baseline === true) {
                            var icon_payload_latest_minuts = '<i class="fas fa-arrow-down" style="color:#ff213c; font-size:25px; padding-top:25%;" data-toggle="tooltip" title="Baseline : '+formatNumber(Math.round(element.latest_minuts_payload_average_baseline * 100) / 100)+'\nActual : '+formatNumber(Math.round(element.latest_minuts_total_payload * 100) / 100)+'\nDelta : '+formatNumber(Math.round(element.latest_minuts_payload_delta_baseline * 100)/100)+'\nGrowth : '+formatNumber(element.today_payload_growth_baseline)+' %"></i>';
                        } else {
                            var icon_payload_latest_minuts = '<i class="fas fa-arrow-up" style="color:#21ff7a; font-size:25px; padding-top:25%;" data-toggle="tooltip" title="Baseline : '+formatNumber(Math.round(element.latest_minuts_payload_average_baseline * 100) / 100)+'\nActual : '+formatNumber(Math.round(element.latest_minuts_total_payload * 100) / 100)+'\nDelta : '+formatNumber(Math.round(element.latest_minuts_payload_delta_baseline * 100)/100)+'\nGrowth : '+formatNumber(element.latest_minuts_payload_growth_baseline)+' %"></i>';
                        }

                        content_frame =
                        `
                        <div class="col-md-6" style="height:50%; padding:0%; padding-left:0%; padding-bottom:15px;">

                            <div class="row" style="padding:0%; margin:0%; height:8%;">

                                <div class="col-md-12" style="padding:0%; padding-left:5px; padding-right:3px">

                                    <div class="card card-color background-gradient-title" style="padding-top:5px">
                                        <div class="row" style="height:100%;">
                                            <div class="col-md-12" style=" padding:0%; padding-left:38px">

                                                <div class="row" style="padding:0%; margin:0%">
                                                    <img src="{{url('/images/`+element.icon+`')}}" alt="Italian Trulli" width="35px" height="80%">
                                                    <h6 style="padding-left:15px">`+element_regional+` | `+element.superpoi+`</h6>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row" style="padding:0%; margin:0%; height:92%">

                                <div class="col-md-3" style="height:100%; padding-bottom:0%; padding-left:5px">
                                    <div class="card card-color">

                                        <div class="card-body" >


                                            <div class="row" style="height:25%; padding-left:7%;">
                                                <h6 style="padding-left:10px;">Last 30 Minuts</h6>
                                                <div class="col-md-8">
                                                    <font style="" color="#ff828f"><i class="fa fa-database" aria-hidden="true"></i> Payload<br>`+formatNumber(Math.round(element.latest_minuts_total_payload * 100) / 100)+` Mb</font>
                                                </div>
                                                <div class="col-md-4">
                                                    `+icon_payload_latest_minuts+`
                                                </div>
                                            </div>

                                            <div class="row" style="height:25%; padding-left:7%; padding-top:7%">
                                                <div class="col-md-8">
                                                    <font color="#90aef1"><i class="fa fa-users" aria-hidden="true"></i> User<br>`+formatNumber(element.latest_minuts_total_user)+`</font></p>
                                                </div>
                                            </div>

                                            <div class="row" style="height:25%; padding-left:7%; padding-top:7%">
                                                <h6 style="padding-left:10px;">Total Today</h6>
                                                <div class="col-md-8">
                                                    <font style="" color="#ff828f"><i class="fa fa-database" aria-hidden="true"></i> Payload<br>`+formatNumber(Math.round(element.today_total_payload * 100) / 100)+` Mb</font>
                                                </div>
                                                <div class="col-md-4">
                                                    `+icon_payload_today+`
                                                </div>
                                            </div>

                                            <div class="row" style="height:25%; padding-left:7%; padding-top:14%">
                                                <div class="col-md-8">
                                                    <font color="#90aef1"><i class="fa fa-users" aria-hidden="true"></i> User<br>`+formatNumber(element.today_total_user)+`</font></p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-9" style="padding-bottom:0%">

                                    <div class="row">

                                        <div class="col-md-12" style="height:70%; padding-top:0%">
                                            <iframe width="100%" height="100%" src="`+element.url_cctv+`">
                                            </iframe>
                                        </div>


                                    <div class="col-md-12" style="height:30%; padding-bottom:0%">
                                        <div class="card card-color text-center">
                                            <div class="card-body" style="padding:0%">
                                                <div id="chartApp-`+element.superpoi+`" style="height:100%; width:100%;"></div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>`

                    $('#content_frame').append(content_frame);
                    var title = "chartApp-"+element.superpoi;
                    chart_tracking_app(title, application_tracking, payload_tracking, user_tracking);
                });

            }
        });
    }

    function chart_tracking_app(title, application_tracking, payload_tracking, user_tracking) {

        var APP_URL = {!! json_encode(url('/images/thanos.png')) !!}

        Highcharts.chart(title, {
            chart: {
                zoomType: 'xy',
                backgroundColor: '#16202c',
            },
            title: {
                text: ''
            },
            subtitle: {
                text: ''
            },
            credits: {
                enabled: false
            },
            // xAxis: [{
                //     categories: application_tracking,
                //     crosshair: true
                // }],
                xAxis: [{
                    categories: application_tracking,
                    crosshair: true,
                    gridLineWidth: 0.5,
                    lineColor: 'transparent',
                    labels: {
                        //align: 'center'
                        useHTML:true,
                        formatter:function(){
                            return '<img class="user-app-icon" alt="Image not found" onerror="this.onerror=null;this.src=' + APP_URL + ';" style="width:20px;" src="images/'+this.value+'.png">';
                        }
                    }
                }],
                yAxis: [{ // Primary yAxis
                    labels: {
                        format: '',
                        style: {
                            color: '#ff828f',
                        }
                    },
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, { // Secondary yAxis
                    title: {
                        text: '',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '',
                        style: {
                            color: '#90aef1',
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    formatter: function() {
                        return '<b>' + this.x + '</b><br/>' +
                        this.series.name + ': ' + this.y + '<br/>'
                    },
                    positioner: function(labelWidth, labelHeight, point) {
                        var tooltipX = point.plotX + 0;
                        var tooltipY = 0;
                        return {
                            x: tooltipX,
                            y: tooltipY
                        };
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'left',
                    x: 0,
                    verticalAlign: 'top',
                    y: 0,
                    floating: true,
                    backgroundColor:
                    Highcharts.defaultOptions.legend.backgroundColor || // theme
                    'rgba(255,255,255,0.25)'
                },
                series: [{
                    showInLegend: false,
                    name: 'User',
                    type: 'column',
                    color: '#3842d7',
                    yAxis: 1,
                    data: user_tracking,
                    tooltip: {
                        valueSuffix: ''
                    }

                }, {
                    showInLegend: false,
                    name: 'Payload',
                    type: 'column',
                    color: '#FF0000',
                    data: payload_tracking,
                    tooltip: {
                        valueSuffix: ' Mb'
                    }
                }]
            });
        }

        //TOTAL PAYLOAD TODAY
        var time_today = [];
        var total_payload = [];
        var total_user = [];

        function get_total_payload_user_today() {
            $.ajax({
                type: "GET",
                url: BaseUrl+'/'+area+'/get-total-payload-user-today',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data){

                    data.forEach(function(element) {
                        time_today.push(element.starttime);
                        total_payload.push(Math.round(element.payload * 100) / 100);
                        total_user.push(parseInt(element.user));
                    });

                    chart_payload_user();
                }
            });
        }

        function chart_payload_user() {

            Highcharts.chart('chart_payload', {
                chart: {
                    type: 'area',
                    backgroundColor: '#16202c',
                },
                accessibility: {
                    description: ''
                },
                title: {
                    text: '<span style="font-size: 11px">'+ formatNumber(total_payload.slice(-1).pop()) + ' Mb</span>',
                    align: 'right',
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: time_today,
                    labels: {
                        style: {
                            fontSize: '7px',
                            color: '#fff'
                        }
                    },

                },
                yAxis: {
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    shared: true,
                    valueSuffix: ' Mb'
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                            stops: [
                            [0, '#f0263e'],
                            [1, '#16202c']
                            ]
                        },
                        marker: {
                            lineWidth: 1,
                        }
                    }
                },
                series: [{
                    showInLegend: false,
                    name: 'Total Payload',
                    data: total_payload,
                    color: '#f0263e',
                }],
            });

            Highcharts.chart('chart_user', {
                chart: {
                    type: 'area',
                    backgroundColor: '#16202c',
                },
                accessibility: {
                    description: ''
                },
                title: {
                    text: '<span style="font-size: 11px">'+ formatNumber(total_user.slice(-1).pop()) + ' User</span>',
                    align: 'right',
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: time_today,
                    labels: {
                        style: {
                            fontSize: '7px',
                            color: '#fff'
                        }
                    },

                },
                yAxis: {
                    title: {
                        text: ''
                    }
                },
                tooltip: {
                    shared: true,
                    valueSuffix: ' User'
                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                            stops: [
                            [0, '#3842d7'],
                            [1, '#16202c']
                            ]
                        },
                        marker: {
                            lineWidth: 1,
                        }
                    }
                },
                series: [{
                    showInLegend: false,
                    name: 'Total User',
                    data: total_user,
                    color: '#3842d7',
                }]
            });
        }

        //TOTAL PAYLOAD TODAY BY APP
        function get_total_payload_user_by_app() {
            $.ajax({
                type: "GET",
                url: BaseUrl+'/'+area+'/get-total-payload-user-by-app',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="X-CSRF-TOKEN"]').attr('content')
                },
                success: function(data){

                    var max_user = [];
                    var max_payload = [];
                    data['data'].forEach(function(element) {
                        max_user.push(element.user);
                        max_payload.push(element.payload);
                    });

                    var isi_table = "";
                    data['data'].forEach(function(element)  {

                        element_application = "<td style='width:50%; padding-left:20px' class='text-left'><img class='user-app-icon' alt='Image not found' style='height:20px; width:20px; padding:0%; margin:0%' src='images/"+element.application+".png'> "+element.application+"</td>";

                        if(element.user == (Math.max.apply(null, max_user))) {
                            element_user    = "<td style='color:#d0ad6f'>"+formatNumber(element.user)+"</td>";
                        } else {
                            element_user    = "<td style='color:#90aef1'>"+formatNumber(element.user)+"</td>";
                        }

                        if(element.payload == (Math.max.apply(null, max_payload))) {
                            element_payload     = "<td style='color:#d0ad6f'>"+formatNumber(Math.round(element.payload * 100) / 100)+"</td>";
                        } else {
                            element_payload     = "<td style='color:#ff828f'>"+formatNumber(Math.round(element.payload * 100) / 100)+"</td>";
                        }

                        //tbody
                        isi_table += "<tr>"+element_application+""+element_user+""+element_payload+"</tr>";

                    });

                    $("#table_value").find('tbody').append(isi_table);

                    //label last updated
                    $('#label_last_updated').html();

                    var last_updated = `<div class="card card-color">
                        <div class="card-body text-right background-gradient-card" style="padding: 5px; font-size:12px; padding-right:10px">
                            <b>Last Updated. `+data['last_update']+`</b>
                        </div>
                    </div>`

                    $('#label_last_updated').html(last_updated);

                }
            });
        }



        //STYLE HIGHT CHART
        Highcharts.theme = {
            colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
            '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
            chart: {
                backgroundColor: {
                    linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
                    stops: [
                    [0, '#2a2a2b'],
                    [1, '#3e3e40']
                    ]
                },
                style: {
                    fontFamily: '\'Unica One\', sans-serif'
                },
                plotBorderColor: '#606063'
            },
            title: {
                style: {
                    color: '#E0E0E3',
                    textTransform: 'uppercase',
                    fontSize: '20px'
                }
            },
            subtitle: {
                style: {
                    color: '#E0E0E3',
                    textTransform: 'uppercase'
                }
            },
            xAxis: {
                gridLineColor: '#707073',
                labels: {
                    style: {
                        color: '#E0E0E3'
                    }
                },
                lineColor: '#707073',
                minorGridLineColor: '#505053',
                tickColor: '#707073',
                title: {
                    style: {
                        color: '#A0A0A3'
                    }
                }
            },
            yAxis: {
                gridLineColor: '#707073',
                labels: {
                    style: {
                        color: '#E0E0E3'
                    }
                },
                lineColor: '#707073',
                minorGridLineColor: '#505053',
                tickColor: '#707073',
                tickWidth: 1,
                title: {
                    style: {
                        color: '#A0A0A3'
                    }
                }
            },
            tooltip: {
                backgroundColor: 'rgba(0, 0, 0, 0.85)',
                style: {
                    color: '#F0F0F0'
                }
            },
            plotOptions: {
                series: {
                    dataLabels: {
                        color: '#F0F0F3',
                        style: {
                            fontSize: '13px'
                        }
                    },
                    marker: {
                        lineColor: '#333'
                    }
                },
                boxplot: {
                    fillColor: '#505053'
                },
                candlestick: {
                    lineColor: 'white'
                },
                errorbar: {
                    color: 'white'
                }
            },
            legend: {
                backgroundColor: 'rgba(0, 0, 0, 0.5)',
                itemStyle: {
                    color: '#E0E0E3'
                },
                itemHoverStyle: {
                    color: '#FFF'
                },
                itemHiddenStyle: {
                    color: '#606063'
                },
                title: {
                    style: {
                        color: '#C0C0C0'
                    }
                }
            },
            credits: {
                style: {
                    color: '#666'
                }
            },
            labels: {
                style: {
                    color: '#707073'
                }
            },
            drilldown: {
                activeAxisLabelStyle: {
                    color: '#F0F0F3'
                },
                activeDataLabelStyle: {
                    color: '#F0F0F3'
                }
            },
            navigation: {
                buttonOptions: {
                    symbolStroke: '#DDDDDD',
                    theme: {
                        fill: '#505053'
                    }
                }
            },
            // scroll charts
            rangeSelector: {
                buttonTheme: {
                    fill: '#505053',
                    stroke: '#000000',
                    style: {
                        color: '#CCC'
                    },
                    states: {
                        hover: {
                            fill: '#707073',
                            stroke: '#000000',
                            style: {
                                color: 'white'
                            }
                        },
                        select: {
                            fill: '#000003',
                            stroke: '#000000',
                            style: {
                                color: 'white'
                            }
                        }
                    }
                },
                inputBoxBorderColor: '#505053',
                inputStyle: {
                    backgroundColor: '#333',
                    color: 'silver'
                },
                labelStyle: {
                    color: 'silver'
                }
            },
            navigator: {
                handles: {
                    backgroundColor: '#666',
                    borderColor: '#AAA'
                },
                outlineColor: '#CCC',
                maskFill: 'rgba(255,255,255,0.1)',
                series: {
                    color: '#7798BF',
                    lineColor: '#A6C7ED'
                },
                xAxis: {
                    gridLineColor: '#505053'
                }
            },
            scrollbar: {
                barBackgroundColor: '#808083',
                barBorderColor: '#808083',
                buttonArrowColor: '#CCC',
                buttonBackgroundColor: '#606063',
                buttonBorderColor: '#606063',
                rifleColor: '#FFF',
                trackBackgroundColor: '#404043',
                trackBorderColor: '#404043'
            }
        };
        // Apply the theme
        Highcharts.setOptions(Highcharts.theme);

    });

</script>

@endsection
