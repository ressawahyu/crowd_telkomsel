<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'Area1Controller@index');

Route::get('/area1', 'Area1Controller@index')->name('area1');
    Route::get('/area1/get-total-payload-user-today', 'Area1Controller@get_total_payload_user_today')->name('area1_get_total_payload_user_today');
    Route::get('/area1/get-total-payload-user-by-app', 'Area1Controller@get_total_payload_user_by_app')->name('area1_get_total_payload_user_by_app');
    Route::get('/area1/get-total-payload-user-by-regional-by-app', 'Area1Controller@get_total_payload_user_by_regional_by_app')->name('area1_get_total_payload_user_by_regional_by_app');
    Route::get('/area1/get-total-payload-user', 'Area1Controller@get_total_payload_and_user')->name('area1_get_total_payload_and_user');


Route::get('/area2', 'Area2Controller@index')->name('area2');
    Route::get('/area2/get-total-payload-user-today', 'Area2Controller@get_total_payload_user_today')->name('area2_get_total_payload_user_today');
    Route::get('/area2/get-total-payload-user-by-app', 'Area2Controller@get_total_payload_user_by_app')->name('area2_get_total_payload_user_by_app');
    Route::get('/area2/get-total-payload-user', 'Area2Controller@get_total_payload_and_user')->name('area2_get_total_payload_and_user');


Route::get('/area3', 'Area3Controller@index')->name('area3');
    Route::get('/area3/get-total-payload-user-today', 'Area3Controller@get_total_payload_user_today')->name('area3_get_total_payload_user_today');
    Route::get('/area3/get-total-payload-user-by-app', 'Area3Controller@get_total_payload_user_by_app')->name('area3_get_total_payload_user_by_app');
    Route::get('/area3/get-total-payload-user-by-regional-by-app', 'Area3Controller@get_total_payload_user_by_regional_by_app')->name('area3_get_total_payload_user_by_regional_by_app');
    Route::get('/area3/get-total-payload-user', 'Area3Controller@get_total_payload_and_user')->name('area3_get_total_payload_and_user');


Route::get('/area4', 'Area4Controller@index')->name('area4');
    Route::get('/area4/get-total-payload-user-today', 'Area4Controller@get_total_payload_user_today')->name('area4_get_total_payload_user_today');
    Route::get('/area4/get-total-payload-user-by-app', 'Area4Controller@get_total_payload_user_by_app')->name('area4_get_total_payload_user_by_app');
    Route::get('/area4/get-total-payload-user-by-regional-by-app', 'Area4Controller@get_total_payload_user_by_regional_by_app')->name('area4_get_total_payload_user_by_regional_by_app');
    Route::get('/area4/get-total-payload-user', 'Area4Controller@get_total_payload_and_user')->name('area4_get_total_payload_and_user');

//Summary Baseline
//Last Thirty Minutes
Route::get('/summary-average-baseline-last-thirty-minutes', 'SummaryAvgBaselineLastThirtyMinutes@index')->name('summary_average_baseline_last_thirty_minutes');
//Today
Route::get('/summary-average-baseline-today', 'SummaryAvgBaselineToday@index')->name('summary_average_baseline_today');
